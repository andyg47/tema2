package service;

public class Zodie {
    public String getNume() {
        return nume;
    }

    public String getZiStart() {
        return ziStart;
    }

    public String getLunaStart() {
        return lunaStart;
    }

    public String getZiFin() {
        return ziFin;
    }

    public String getLunaFin() {
        return lunaFin;
    }

    private String nume;
    private String ziStart;
    private String lunaStart;
    private String ziFin;
    private String lunaFin;


    public Zodie(String nume, String zstart, String lstart, String zfin, String lfin) {
        this.nume = nume;
        this.ziStart = zstart;
        this.lunaStart = lstart;
        this.ziFin = zfin;
        this.lunaFin = lfin;
    }
}
