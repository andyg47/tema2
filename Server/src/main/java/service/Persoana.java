package service;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import proto.PersoanaGrpc;
import proto.PersoanaOuterClass;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Persoana extends PersoanaGrpc.PersoanaImplBase {

    @Override
    public void prelucrareZodie (PersoanaOuterClass.RequestData request, StreamObserver < PersoanaOuterClass.ZodieReply > responseObserver) {
        PersoanaOuterClass.ZodieReply.Builder response = PersoanaOuterClass.ZodieReply.newBuilder();
        ArrayList<Zodie> zodii = new ArrayList<>();
        String data = String.valueOf(request);
        try {
            zodii = citire();
            if(cautare(data, zodii)!="")
            {
                String zodiaPersoanei = cautare(data, zodii);
                System.out.println("Zodia este: " + zodiaPersoanei);
            }
            else {
                System.out.println("Eroare. Data nu corespunde niciunei zodii.");
            }

            response.setMessage("ok");
            responseObserver.onNext(response.build());
            responseObserver.onCompleted();

        } catch (FileNotFoundException e) {
            Status status = Status.NOT_FOUND.withDescription("Fisierul nu a fost gasit!");

            e.printStackTrace();
        }
        PersoanaOuterClass.ZodieReply reply = PersoanaOuterClass.ZodieReply.newBuilder().setMessage("Operatie efectuata cu succes.").build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }

    private ArrayList<Zodie> citire() throws FileNotFoundException {
        ArrayList<Zodie> zodii = new ArrayList<>();
        try {
            File file = new File("C:\\Users\\Andy\\Desktop\\tema2\\Server\\src\\resurse\\date.txt");
            Scanner sc = new Scanner(file);
            while (sc.hasNext()) {
                String nume = sc.next();
                String start = sc.next();
                String fin = sc.next();
                String zstart="";
                zstart+= start.charAt(0);
                zstart+= start.charAt(1);
                String lstart="";
                lstart+= start.charAt(3);
                lstart+= start.charAt(4);
                String zfin="";
                zfin+= fin.charAt(0);
                zfin+= fin.charAt(1);
                String lfin="";
                lfin+= fin.charAt(3);
                lfin+= fin.charAt(4);
                Zodie aux = new Zodie(nume, zstart, lstart, zfin, lfin);
                zodii.add(aux);
            }
        } catch (FileNotFoundException e) {
            System.out.println("ERROR" + e);
        } catch (Exception e) {
            System.out.println(e);
        }
        return zodii;
    }

    public String cautare(String data, ArrayList<Zodie> zodii) {

        String dataT = data;
        String ziTemp ="";
        ziTemp += dataT.charAt(0);
        ziTemp += dataT.charAt(1);
        String lunaTemp="";
        lunaTemp+= dataT.charAt(3);
        lunaTemp+= dataT.charAt(4);
        for (int index = 0; index < zodii.size(); index++) {
            if((Integer.parseInt(lunaTemp)== Integer.parseInt(zodii.get(index).getLunaStart()) && Integer.parseInt(ziTemp)>= Integer.parseInt(zodii.get(index).getZiStart())) || (Integer.parseInt(lunaTemp)== Integer.parseInt(zodii.get(index).getLunaFin()) && Integer.parseInt(ziTemp)<= Integer.parseInt(zodii.get(index).getZiFin()))) {
                return zodii.get(index).getNume();
                }
            }
        return "";
        }


    }