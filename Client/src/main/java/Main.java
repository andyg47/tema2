import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import proto.PersoanaGrpc;
import proto.PersoanaOuterClass;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.InputMismatchException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.Scanner;

    public class Main {
        public static boolean verificare(String date) {
            try {
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            dateFormat.setLenient(false);
            dateFormat.parse(date);
            return true;
            } catch (InputMismatchException e) {
            System.out.println(e);
            return false;
            } catch (ParseException e) {
            System.out.println(e);
            return false;
            } catch (DateTimeException e) {
            System.out.println(e);
            return false;
            }
    }

    public static void main(String[] args) {

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 4444).usePlaintext().build();

        PersoanaGrpc.PersoanaBlockingStub persoanaStub = PersoanaGrpc.newBlockingStub(channel);
        boolean running = true;
        while(running) {
            boolean ok = true;
            Scanner sc = new Scanner(System.in);
            System.out.println("Introduceti data nasterii in format mm/dd/yyyy: ");
            String data = sc.nextLine();
            if (verificare(data) == false) {
                ok = false;
            }

            if (ok) {
                persoanaStub.prelucrareZodie(PersoanaOuterClass.RequestData.newBuilder().setData(data).build());
                System.out.println("Date trimise cu succes.");
            } else System.out.println("Data furnizata este invalida,si nu a fost trimisa.");
        }

        channel.shutdown();
    }
}
